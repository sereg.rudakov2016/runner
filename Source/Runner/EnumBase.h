// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
#include "Engine/DataTable.h"
#include "CoreMinimal.h"
#include "EnumBase.generated.h"

UENUM(BlueprintType)
enum class EEnumTabMenu : uint8
{
	MenuStart           UMETA(DisplayName = "MenuStart"),
	MenuAutorisation    UMETA(DisplayName = "MenuAutorisation"),
	MenuRegestration	UMETA(DisplayName = "MenuRegestration"),
	MenuMain	        UMETA(DisplayName = "MenuMain"),
	MenuRecord	        UMETA(DisplayName = "MenuRecord"),
	MenuChangeHero	    UMETA(DisplayName = "MenuChangeHero"),
	MenuLoad	        UMETA(DisplayName = "MenuLoad")
};

UENUM(BlueprintType)
enum class EEnumTypeInteract : uint8
{
	None            UMETA(DisplayName = "None"),
	Tutorial        UMETA(DisplayName = "Tutorial"),
	Bonus           UMETA(DisplayName = "Bonus"),
	Obstacle	    UMETA(DisplayName = "Obstacle"),
	Boost	        UMETA(DisplayName = "Boost"),
	Finish	        UMETA(DisplayName = "Finish"),
	BonusCollector  UMETA(DisplayName = "BonusCollector"),
	Destroyer       UMETA(DisplayName = "Destroyer"),
	ObstacleJamp    UMETA(DisplayName = "ObstacleJamp"),
	PatchBase       UMETA(DisplayName = "PatchBase")
};

USTRUCT(BlueprintType)
struct FStrTypeLinePatch
{
	GENERATED_USTRUCT_BODY()
public:

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		EEnumTypeInteract TypeRight;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		bool isUPRight;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		EEnumTypeInteract TypeMiddle;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		bool isUPMiddle;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		EEnumTypeInteract TypeLeft;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		bool isUPLeft;
};

USTRUCT(BlueprintType)
struct FStrPresetPatch : public FTableRowBase
{
	GENERATED_USTRUCT_BODY()
public:

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TArray<FStrTypeLinePatch> Patch;


};
